<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Imports\SeedImport;
use Maatwebsite\Excel\Facades\Excel;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $import = new SeedImport();
        Excel::import($import, base_path().'/database/seeds/BackEndTest_TestData_v1.1.xlsx');
    }
}
