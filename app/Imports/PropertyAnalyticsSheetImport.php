<?php
namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\PropertyAnalytic;
use Illuminate\Support\Str;

class PropertyAnalyticsSheetImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        return new PropertyAnalytic([
            'property_id' => $row['property_id'],
            'analytic_type_id'    => $row['anaytic_type_id'],
            'value'    => $row['value'],
        ]);
    }
}
