<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class SeedImport implements WithMultipleSheets 
{
   
    public function sheets(): array
    {
        return [
            0 => new PropertiesSheetImport(),
            1 => new AnalyticTypesSheetImport(),
            2 => new PropertyAnalyticsSheetImport(),
        ];
    }
}
