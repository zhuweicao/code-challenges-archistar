<?php
namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\AnalyticType;
use Illuminate\Support\Str;

class AnalyticTypesSheetImport implements ToModel, WithHeadingRow
{

    public function model(array $row)
    {
        return new AnalyticType([
            'name' => $row['name'],
            'units'    => $row['units'],
            'is_numeric'    => $row['is_numeric'],
            'num_decimal_places'    => $row['num_decimal_places'],
        ]);
    }
}
