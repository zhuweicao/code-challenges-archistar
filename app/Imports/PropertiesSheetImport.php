<?php
namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\Property;
use Illuminate\Support\Str;

class PropertiesSheetImport implements ToModel, WithHeadingRow
{

    public function model(array $row)
    {
        return new Property([
            'suburb' => $row['suburb'],
            'state'    => $row['state'],
            'country'    => $row['counrty'],
            'guid'    => Str::uuid()->toString(),
        ]);
    }
}
