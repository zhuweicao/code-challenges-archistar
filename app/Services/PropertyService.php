<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Property;
use App\Models\PropertyAnalytic;
use App\Models\AnalyticType;

class PropertyService
{
    /**
     * Insert a new property.
     *
     * @return response
     */
    public function newProperty($data)
    {
        try {
            // populate data and save to property table
            $property = new Property();
            $property->suburb = $data->suburb;
            $property->state = $data->state;
            $property->country = $data->country;
            $property->guid = Str::uuid()->toString();
            $property->save();
            return response()->json([
                'message' => 'Property created.'
            ],201);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ],400);
        }
    }

    /**
     * Make a new analytic for a property.
     *
     * @return response
     */
    public function newAnalytic($data)
    {
        try {
            // populate data and insert into property analytic table
            $propertyAnalytic = new PropertyAnalytic();
            $propertyAnalytic->property_id = $data->property_id;
            $propertyAnalytic->analytic_type_id = $data->analytic_type_id;
            $propertyAnalytic->value = $data->value;
            $propertyAnalytic->save();
            return response()->json([
                'message' => 'Property analytic saved.'
            ],201);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ],400);
        }
    }

    /**
     * Update an analytic for a property.
     *
     * @return response
     */
    public function analyticUpdate($propertyAnalytic, $data)
    {
        try {
            // populate data and save to property analytic table
            $propertyAnalytic->property_id = $data->property_id ?? $propertyAnalytic->property_id;
            $propertyAnalytic->analytic_type_id = $data->analytic_type_id ?? $propertyAnalytic->analytic_type_id;
            $propertyAnalytic->value = $data->value ?? $propertyAnalytic->value;
            $propertyAnalytic->save();
            return response()->json([
                'message' => 'Property analytic updated.'
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ],400);
        }
    }

    /**
     * Get a summary of all property analytics for an inputted value.
     * min value, max value, median value, percentage properties with a value, percentage properties without a value
     * 
     * @return response
     */
    public function analyticsSummary($data)
    {
        try {
            $propertyAnalytics = PropertyAnalytic::whereHas('property', function ($query) use ($data) {
                $query->where($data->group_field, $data->value);
            })->get();
            $minValue = $propertyAnalytics->min('value');
            $maxValue = $propertyAnalytics->max('value');
            $avgValue = $propertyAnalytics->avg('value');
            // convert the field data and the request data to lower case
            $propertiesNumber = Property::whereRaw("LOWER(" . $data->group_field . ")='" . strtolower($data->value) . "'")->get()->count();
            $propertiesWithValue = Property::whereRaw("LOWER(" . $data->group_field . ")='". strtolower($data->value) . "'")->whereHas('analytics')->get()->count();
            $propertiesWithOutValue = $propertiesNumber - $propertiesWithValue;
            $percentPropertiesWithValue = $propertiesNumber ? round($propertiesWithValue / $propertiesNumber * 100, 2) . '%' : null;
            $percentPropertiesWithOutValue = $propertiesNumber ? round($propertiesWithOutValue / $propertiesNumber * 100, 2) . '%' : null;
            return response()->json([
                'message' => compact('minValue', 'maxValue', 'avgValue', 'percentPropertiesWithValue', 'percentPropertiesWithOutValue')
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ],400);
        }
    }
}
