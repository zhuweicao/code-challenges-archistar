<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\PropertyService;

class PropertyServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('PropertyService', function () {
            return new PropertyService();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
