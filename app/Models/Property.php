<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'suburb',
        'state',
        'country',
        'guid',
        'created_at',
        'updated_at'
    ];

    /**
     * Get the analytics for the property.
     */
    public function analytics()
    {
        return $this->hasMany('App\Models\PropertyAnalytic', 'property_id', 'id');
    }
}
