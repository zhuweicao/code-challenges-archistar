<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyAnalytic extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'property_id',
        'analytic_type_id',
        'value',
        'created_at',
        'updated_at'
    ];

    /**
     * Get the property record associated with the property analytic.
     */
    public function property()
    {
        return $this->hasOne('App\Models\Property', 'id', 'property_id');
    }

    /**
     * Get the analytic type record associated with the property analytic.
     */
    public function analyticType()
    {
        return $this->hasOne('App\Models\AnalyticType', 'id', 'analytic_type_id');
    }
}
