<?php

namespace App\Http\Controllers;

use PropertyService;
use Illuminate\Http\Request;
use App\Models\Property;
use App\Models\PropertyAnalytic;
use App\Models\AnalyticType;

class PropertyController extends Controller
{
    /**
     * Insert new property
     *
     * params in request
     * 'suburb' => 'required|alpha',
     * 'state' => 'required|alpha',
     * 'country' => 'required|alpha',
     * 
     * @return response
     */
    public function new(Request $request)
    {
        try {
            // validate the format of the input data
            $request->validate([
                'suburb' => 'required|alpha',
                'state' => 'required|alpha',
                'country' => 'required|alpha',
            ]);
        } catch (\Illuminate\Validation\ValidationException $e) {
            return response()->json([
                'message' => $e->errors()
            ],400);
        }

        return PropertyService::newProperty($request);
    }

    /**
     * Insert new analytic for a property
     *
     * params in request
     * 'property_id' => 'required|integer',
     * 'analytic_type_id' => 'required|integer',
     * 'value' => 'required|numeric',
     * 
     * @return response
     */
    public function analytic(Request $request)
    {
        try {
            // validate the format of the input data
            $request->validate([
                'property_id' => ['required', 'integer', function ($attribute, $value, $fail) {
                    // check if property_id is valid
                    if (!Property::find($value)) {
                        $fail($attribute.' is invalid.');
                    }
                }],
                'analytic_type_id' => ['required', 'integer',function ($attribute, $value, $fail) {
                    // check if analytic_type_id id is valid
                    if (!AnalyticType::find($value)) {
                        $fail($attribute.' is invalid.');
                    }
                }],
                'value' => 'required|numeric',
            ]);
        } catch (\Illuminate\Validation\ValidationException $e) {
            return response()->json([
                'message' => $e->errors()
            ],400);
        }
        
        return PropertyService::newAnalytic($request);
    }

    /**
     * Update an analytic for a property
     *
     * params in request
     * 'property_id' => 'optional|integer',
     * 'analytic_type_id' => 'optional|integer',
     * 'value' => 'optional|numeric',
     * 
     * @return response
     */
    public function analyticUpdate($id, Request $request)
    {
        try {
            $propertyAnalytic = PropertyAnalytic::find($id);

            // validate the format of the input data
            if (!$propertyAnalytic)
                return response()->json([
                    'message' => 'Property analytic Id is invalid.'
                ],400);

            $request->validate([
                'property_id' => ['integer', function ($attribute, $value, $fail) {
                    // check if property_id is valid
                    if ($value && !Property::find($value)) {
                        $fail($attribute.' is invalid.');
                    }
                }],
                'analytic_type_id' => ['integer',function ($attribute, $value, $fail) {
                    // check if analytic_type_id id is valid
                    if ($value && !AnalyticType::find($value)) {
                        $fail($attribute.' is invalid.');
                    }
                }],
                'value' => 'numeric',
            ]);
        } catch (\Illuminate\Validation\ValidationException $e) {
            return response()->json([
                'message' => $e->errors()
            ],400);
        }

        return PropertyService::analyticUpdate($propertyAnalytic, $request);
    }

    /**
     * Update an analytic for a property
     *
     * params in request
     * 'group_field' => 'required|alpha',
     * 'value' => 'required|alpha',
     *
     * @return response
     */
    public function analyticsSummary(Request $request)
    {
        try {
            $request->validate([
                'group_field' => ['required', 'alpha', function ($attribute, $value, $fail) {
                    // check if property_id is valid
                    $validFields = [
                        'suburb',
                        'state',
                        'country'
                    ];
                    if (!in_array($value, $validFields)) {
                        $fail($attribute.' is invalid.');
                    }
                }],
                'value' => 'required|alpha'
            ]);
        } catch (\Illuminate\Validation\ValidationException $e) {
            return response()->json([
                'message' => $e->errors()
            ],400);
        }

        return PropertyService::analyticsSummary($request);
    }
}
