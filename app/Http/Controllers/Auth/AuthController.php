<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth; 
use \Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * Login function
     * 
     * params in request
     * 'email' => 'required|string|email'
     * 'password' => 'required|string'
     *
     * @return response
     */
    public function login(Request $request) 
    {
        try {
            // validate the format of the input data
            $request->validate([
                'email' => 'required|string|email',
                'password' => 'required|string'
            ]);
        } catch (ValidationException $e) {
            return response()->json([
                'message' => $e->errors()
            ],400);
        }

        try {
            $credentials = request(['email', 'password']);
            // try to authenticate user
            if(!Auth::attempt($credentials))
                return response()->json([
                    'message' => 'Unauthorized'
                ],401);

            // generate token and response
            $user = $request->user();
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            $token->save();
            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ],400);
        }
    }

    /**
     * Register function
     * 
     * params in request
     * 'name' => 'required|string'
     * 'email' => 'required|string|email|unique:users'
     * 'password' => 'required|string'
     *
     * @return response
     */
    public function register(Request $request)
    {
        try {
            // validate the format of the input data
            $request->validate([
                'name' => 'required|string',
                'email' => 'required|string|email|unique:users',
                'password' => 'required|string'
            ]);
        } catch (ValidationException $e) {
            return response()->json([
                'message' => $e->errors()
            ],400);
        }

        try {
            // save details to user table
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();
            return response()->json([
                'message' => 'Successfully created user!'
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ],400);
        }
    }

    /**
     * Logout function
     *
     * @return response
     */
    public function logout(Request $request)
    {
        $request->user('api')->token()->revoke();
        return response()->json([
          'message' => 'Successfully logged out'
        ], 200);
    }
    
}
