#!/bin/bash
composer install
npm install
touch database/database.sqlite
touch database/test.sqlite

ABS_PATH="$(pwd)"/"$(basename "$1")database/database.sqlite"
searchEscaped=$(sed 's/\//\\\//g; s/\./\\\./g' <<<"$ABS_PATH") # escape it.
sed "s/\/real\/path\/to\/database\/database\.sqlite/$searchEscaped/g" .env.example > .env

TESTING_ABS_PATH="$(pwd)"/"$(basename "$1")database/test.sqlite"
searchEscaped=$(sed 's/\//\\\//g; s/\./\\\./g' <<<"$TESTING_ABS_PATH") # escape it.
sed "s/\/real\/path\/to\/database\/database\.sqlite/$searchEscaped/g" .env.example > .env.testing
php artisan migrate && php artisan db:seed
php artisan passport:install
