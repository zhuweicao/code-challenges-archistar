<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Services\PropertyService;
use App\Models\PropertyAnalytic;

class PropertyTest extends TestCase
{
    /**
     * Unit test for adding new property.
     *
     * @return void
     */
    public function testNewProperty()
    {
        $data = new \stdClass();
        $data->suburb = 'mascot';
        $data->state = 'nsw';
        $data->country = 'australia';
        $propertyService = new PropertyService();
        $response = $propertyService->newProperty($data);
        $this->assertEquals($response->getStatusCode(), '201');

        $data = new \stdClass();
        $data->suburb = 'Sydney';
        $data->state = 'nsW';
        $data->country = 'australiA';
        $propertyService = new PropertyService();
        $response = $propertyService->newProperty($data);
        $this->assertEquals($response->getStatusCode(), '201');

        $data = new \stdClass();
        $data->suburb = '';
        $data->state = 'nsW';
        $data->country = 'australiA';
        $propertyService = new PropertyService();
        $response = $propertyService->newProperty($data);
        $this->assertEquals($response->getStatusCode(), '201');

        $data = new \stdClass();
        $data->suburb = '';
        $data->country = 'australiA';
        $propertyService = new PropertyService();
        $response = $propertyService->newProperty($data);
        $this->assertEquals($response->getStatusCode(), '400');

        $data = new \stdClass();
        $data->state = 'nsW';
        $data->country = 'australiA';
        $propertyService = new PropertyService();
        $response = $propertyService->newProperty($data);
        $this->assertEquals($response->getStatusCode(), '400');

        $data = new \stdClass();
        $data->state = 'nsW';
        $propertyService = new PropertyService();
        $response = $propertyService->newProperty($data);
        $this->assertEquals($response->getStatusCode(), '400');
    }

    /**
     * Unit test for adding new property analytic.
     *
     * @return void
     */
    public function testNewAnalytic()
    {
        $data = new \stdClass();
        $data->property_id = '1';
        $data->analytic_type_id = '2';
        $data->value = '5';
        $propertyService = new PropertyService();
        $response = $propertyService->newAnalytic($data);
        $this->assertEquals($response->getStatusCode(), '201');

        $data = new \stdClass();
        $data->property_id = '1';
        $data->analytic_type_id = '5';
        $data->value = '-5';
        $propertyService = new PropertyService();
        $response = $propertyService->newAnalytic($data);
        $this->assertEquals($response->getStatusCode(), '201');

        $data = new \stdClass();
        $data->property_id = '1';
        $data->analytic_type_id = '2';
        $propertyService = new PropertyService();
        $response = $propertyService->newAnalytic($data);
        $this->assertEquals($response->getStatusCode(), '400');

        $data = new \stdClass();
        $data->property_id = '1';
        $data->value = '5';
        $propertyService = new PropertyService();
        $response = $propertyService->newAnalytic($data);
        $this->assertEquals($response->getStatusCode(), '400');
    }

    /**
     * Unit test for updating property analytic.
     *
     * @return void
     */
    public function testAalyticUpdate()
    {
        $propertyAnalytic = PropertyAnalytic::find(1);
        $data = new \stdClass();
        $data->property_id = '2';
        $data->analytic_type_id = '3';
        $data->value = '123';
        $propertyService = new PropertyService();
        $response = $propertyService->analyticUpdate($propertyAnalytic, $data);
        $propertyAnalytic = PropertyAnalytic::find(1);
        $this->assertEquals($propertyAnalytic->property_id, '2');
        $this->assertEquals($propertyAnalytic->analytic_type_id, '3');
        $this->assertEquals($propertyAnalytic->value, '123');

        $propertyAnalytic = PropertyAnalytic::find(1);
        $data = new \stdClass();
        $data->property_id = '2';
        $data->analytic_type_id = '3';
        $data->value = 'aasdas';
        $propertyService = new PropertyService();
        $response = $propertyService->analyticUpdate($propertyAnalytic, $data);
        $propertyAnalytic = PropertyAnalytic::find(1);
        $this->assertEquals($propertyAnalytic->property_id, '2');
        $this->assertEquals($propertyAnalytic->analytic_type_id, '3');
        $this->assertEquals($propertyAnalytic->value, 'aasdas');

        $propertyAnalytic = PropertyAnalytic::find(1);
        $data = new \stdClass();
        $data->property_id = '2';
        $data->analytic_type_id = '55';
        $data->value = 'aasdas';
        $propertyService = new PropertyService();
        $response = $propertyService->analyticUpdate($propertyAnalytic, $data);
        $propertyAnalytic = PropertyAnalytic::find(1);
        $this->assertEquals($propertyAnalytic->property_id, '2');
        $this->assertEquals($propertyAnalytic->analytic_type_id, '55');
        $this->assertEquals($propertyAnalytic->value, 'aasdas');

        $propertyAnalytic = PropertyAnalytic::find(1);
        $data = new \stdClass();
        $data->property_id = '99999';
        $propertyService = new PropertyService();
        $response = $propertyService->analyticUpdate($propertyAnalytic, $data);
        $propertyAnalytic = PropertyAnalytic::find(1);
        $this->assertEquals($propertyAnalytic->property_id, '99999');
        $this->assertEquals($propertyAnalytic->analytic_type_id, '55');
        $this->assertEquals($propertyAnalytic->value, 'aasdas');

        $propertyAnalytic = PropertyAnalytic::find(0);
        $data = new \stdClass();
        $data->property_id = '99999';
        $propertyService = new PropertyService();
        $response = $propertyService->analyticUpdate($propertyAnalytic, $data);
        $this->assertEquals($response->getStatusCode(), '400');
    }

    /**
     * Unit test for updating property analytic.
     *
     * @return void
     */
    public function testAnalyticsSummary()
    {
        $data = new \stdClass();
        $data->group_field = '2';
        $data->value = '3';
        $propertyService = new PropertyService();
        $response = $propertyService->analyticsSummary($data);
        $this->assertEquals($response->getStatusCode(), '200');

        $data = new \stdClass();
        $data->group_field = 'suburb';
        $data->value = 'Richmond';
        $propertyService = new PropertyService();
        $response = $propertyService->analyticsSummary($data);
        $this->assertEquals($response->getStatusCode(), '200');

        $data = new \stdClass();
        $data->value = '3';
        $propertyService = new PropertyService();
        $response = $propertyService->analyticsSummary($data);
        $this->assertEquals($response->getStatusCode(), '400');

        $data = new \stdClass();
        $data->group_field = '2';
        $propertyService = new PropertyService();
        $response = $propertyService->analyticsSummary($data);
        $this->assertEquals($response->getStatusCode(), '400');
    }

}
