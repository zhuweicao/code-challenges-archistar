<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'App\Http\Controllers\Auth\AuthController@login')->name('login');
    Route::post('register', 'App\Http\Controllers\Auth\AuthController@register')->name('register');
    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'App\Http\Controllers\Auth\AuthController@logout')->name('logout');
    });
});

Route::group([
    'prefix' => 'property'
], function () {
    Route::group(['middleware' => 'auth:api'], function() {
        Route::post('/new', 'App\Http\Controllers\PropertyController@new')->name('property.new');
        Route::post('/analytic', 'App\Http\Controllers\PropertyController@analytic')->name('property.analytic');
        Route::put('/analytic/{id}', 'App\Http\Controllers\PropertyController@analyticUpdate')->name('property.analytic.update');
        Route::get('/analytics/summary', 'App\Http\Controllers\PropertyController@analyticsSummary')->name('property.analytics.summary');
    });
});
