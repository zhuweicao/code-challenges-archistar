<h2 id="instructions">Instructions</h2>
<p><strong>Please run the script <code>init.sh</code> under folder <code>property-api</code> to initialize the project.</strong>
<strong>Then go to property-api folder and Run cmd <code>php artisan serve --port=8080</code> to start the local server of this app.</strong></p>
<p><strong>Endpoints:</strong></p>
<ol>
<li><p><a href="http://127.0.0.1:8080/api/auth/register">http://127.0.0.1:8080/api/auth/register</a>
<br>
 Method: <code>POST</code>
<br>
 Params:
<br>
 &ensp;&ensp;&ensp;&ensp;<code>name:required</code>
<br>
 &ensp;&ensp;&ensp;&ensp;<code>email:required</code>
<br>
 &ensp;&ensp;&ensp;&ensp;<code>password:required</code>
<br>
 Description: Post your <code>name</code>, <code>email</code> and <code>password</code> to url <code>http://127.0.0.1:8080/api/auth/register</code> to register an api user.</p>
</li>
<li><p><a href="http://127.0.0.1:8080/api/auth/login">http://127.0.0.1:8080/api/auth/login</a>
<br>
 Method: <code>POST</code>
<br>
 Params:
<br>
 &ensp;&ensp;&ensp;&ensp;<code>email:required</code>
<br>
 &ensp;&ensp;&ensp;&ensp;<code>password:required</code>
<br>
 Description: Post your <code>email</code> and <code>password</code> to <code>http://127.0.0.1:8080/api/auth/login</code> to login and get the Bearer <code>access_token</code>.</p>
</li>
<li><p><a href="http://127.0.0.1:8080/api/auth/logout">http://127.0.0.1:8080/api/auth/logout</a>
<br>
 Method: <code>GET</code>
<br>
 Header: <code>Authorization=&#39;Bearer [access_token]&#39;</code></p>
</li>
<li><p><a href="http://127.0.0.1:8080/api/property/new">http://127.0.0.1:8080/api/property/new</a>
<br>
 Method: <code>POST</code>
<br>
 Header: <code>Authorization=&#39;Bearer [access_token]&#39;</code>
<br>
 Params:
<br>
 &ensp;&ensp;&ensp;&ensp;<code>suburb:required</code>
<br>
 &ensp;&ensp;&ensp;&ensp;<code>state:required</code>
<br>
 &ensp;&ensp;&ensp;&ensp;<code>country:required</code>
<br>
 Description: Add new property to DB</p>
</li>
<li><p><a href="http://127.0.0.1:8080/api/property/analytic">http://127.0.0.1:8080/api/property/analytic</a>
<br>
 Method: <code>POST</code>
<br>
 Header: <code>Authorization=&#39;Bearer [access_token]&#39;</code>
<br>
 Params:
<br>
 &ensp;&ensp;&ensp;&ensp;<code>property_id:required</code>
<br>
 &ensp;&ensp;&ensp;&ensp;<code>analytic_type_id:required</code>
<br>
 &ensp;&ensp;&ensp;&ensp;<code>value:required</code>
<br>
 Description: Add new property analytic to DB</p>
</li>
<li><p><a href="http://127.0.0.1:8080/api/property/analytic/{id}">http://127.0.0.1:8080/api/property/analytic/{id}</a>
<br>
 Method: <code>PUT</code>
<br>
 Header: <code>Authorization=&#39;Bearer [access_token]&#39;</code>
<br>
 Params:
<br>
 &ensp;&ensp;&ensp;&ensp;<code>property_id:optional</code>
<br>
 &ensp;&ensp;&ensp;&ensp;<code>analytic_type_id:optional</code>
<br>
 &ensp;&ensp;&ensp;&ensp;<code>value:optional</code>
<br>
 Description: Update a property analytic in DB, the <code>id</code> in url is <code>required</code>, it is a property analytic id. Please note <code>PUT</code> method <strong>might not</strong> work if you send <code>form-data</code> instead of <code>x-www-form-urlencoded</code> data.</p>
</li>
<li><p><a href="http://127.0.0.1:8080/api/property/analytics/summary?group_field={field}&amp;value={field_value}">http://127.0.0.1:8080/api/property/analytics/summary?group_field={field}&amp;value={field_value}</a>
<br>
 Method: <code>GET</code>
<br>
 Header: <code>Authorization=&#39;Bearer [access_token]&#39;</code>
<br>
 Description: Get a summary of all property analytics for an inputted <code>suburb/state/country</code> (min value, max value, median value, percentage properties with a value, percentage properties without a value). <code>group_field</code> is the field you want to search<code>(suburb/state/country)</code>, <code>field_value</code> is the value for the search fields.</p>
</li>
</ol>
<p><strong>Unit test</strong>
<br>
There are some basic unit tests in this project, you can run <code>php artisan test</code> to run those tests.</p>
