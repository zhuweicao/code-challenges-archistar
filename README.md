## Instructions

**Please run the script `init.sh` under the root folder to initialize the project.**
**Then run cmd `php artisan serve --port=8080` to start the local server of this app.**

**Endpoints:**

1. http://127.0.0.1:8080/api/auth/register  
Method: `POST`  
Params:  
- `name:required`  
- `email:required`  
- `password:required`  
Description: Post your `name`, `email` and `password` to url `http://127.0.0.1:8080/api/auth/register` to register an api user.  

2. http://127.0.0.1:8080/api/auth/login  
Method: `POST`  
Params:  
- `email:required`  
- `password:required`  
Description: Post your `email` and `password` to `http://127.0.0.1:8080/api/auth/login` to login and get the Bearer `access_token`.  

3. http://127.0.0.1:8080/api/auth/logout  
Method: `GET`  
Header: `Authorization='Bearer [access_token]'`  

4. http://127.0.0.1:8080/api/property/new  
Method: `POST`  
Header: `Authorization='Bearer [access_token]'`  
Params:  
- `suburb:required`  
- `state:required`  
- `country:required`  
Description: Add new property to DB  

5. http://127.0.0.1:8080/api/property/analytic  
Method: `POST`  
Header: `Authorization='Bearer [access_token]'`  
Params:  
- `property_id:required`  
- `analytic_type_id:required`  
- `value:required`  
Description: Add new property analytic to DB  

6. http://127.0.0.1:8080/api/property/analytic/{id}  
Method: `PUT`  
Header: `Authorization='Bearer [access_token]'`  
Params:  
- `property_id:optional`  
- `analytic_type_id:optional`  
- `value:optional`  
Description: Update a property analytic in DB, the `id` in url is `required`, it is a property analytic id. Please note `PUT` method **might not** work if you send `form-data` instead of `x-www-form-urlencoded` data.  

7. http://127.0.0.1:8080/api/property/analytics/summary?group_field={field}&value={field_value}  
Method: `GET`  
Header: `Authorization='Bearer [access_token]'`  
Description: Get a summary of all property analytics for an inputted `suburb/state/country` (min value, max value, median value, percentage properties with a value, percentage properties without a value). `group_field` is the field you want to search`(suburb/state/country)`, `field_value` is the value for the search fields.  

**Unit test**
There are some basic unit tests in this project, you can run `php artisan test` to run those tests.
